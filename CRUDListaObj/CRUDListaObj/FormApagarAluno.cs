﻿namespace CRUDListaObj
{
    using CRUDListaObj.Modelos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarAluno : Form
    {
        private List<Aluno> ListaDeAlunos = new List<Aluno>();
        private Form1 FormPrincipal;
        private Aluno alunoAApagar;
        public FormApagarAluno(List<Aluno>ListaDeAlunos, Aluno alunoAApagar)
        {
            InitializeComponent();
            this.ListaDeAlunos = ListaDeAlunos;
            this.alunoAApagar = alunoAApagar;
           

            dataGridViewAlunoApagar.Rows.Add(
                alunoAApagar.IdAluno,
                alunoAApagar.PrimeiroNome,
                alunoAApagar.Apelido);

            dataGridViewAlunoApagar.AllowUserToAddRows = false;
        }

        private void BotaoCancelar_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("O Aluno não foi apagado");
            Close();

        }

        private void BotaoApagar_Click(object sender, System.EventArgs e)
        {
            int index = 0;
            foreach (var aluno in ListaDeAlunos)
            {
                if(alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index);
                    break;
                }
                index++;
            }
            MessageBox.Show("Aluno apagado com sucesso");
       
        }
    }
}
