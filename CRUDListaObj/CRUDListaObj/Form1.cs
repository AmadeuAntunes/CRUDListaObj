﻿namespace CRUDListaObj
{
    using Modelos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System;
    using System.IO;

    public partial class Form1 : Form
    {
        private List<Aluno> ListaDeAlunos;

        private FormNovoAluno furmularioNovoAluno;

        private FormApagarAluno formularioApagaraluno;

        private Form1 formPrincipal = new Form1();
        public Form1()
        {
           
            InitializeComponent();
            ListaDeAlunos = new List<Aluno>();
            if (!CarregarAlunos())
            {
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Amadeu", Apelido = "Antunes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16328, PrimeiroNome = "António", Apelido = "Almeida" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 14990, PrimeiroNome = "Daniel", Apelido = "Veiga" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16342, PrimeiroNome = "João", Apelido = "Ribeiro" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16333, PrimeiroNome = "Luís", Apelido = "Gomes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 12494, PrimeiroNome = "Hugo", Apelido = "Pereira" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16334, PrimeiroNome = "Nuno", Apelido = "Serrano" });
            }

            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            foreach (var aluno in ListaDeAlunos)
            {
                ComboNomes.Items.Add(aluno.PrimeiroNome);
            }
            
            formPrincipal = this;
        }

        private void ButtonProcurar_Click(object sender, System.EventArgs e)
        {
            if(ComboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show("Escolha um Critário");
                return;
            }
            int id = 0;
            if(ComboBoxProcurar.SelectedIndex == 0)
            {
                if(!int.TryParse(TextBoxProcurar.Text, out id))
                {
                    MessageBox.Show("Tem de escolher um valor numérico");

                }

                var alunoAchado = ProcurarAluno(id);
                if(alunoAchado != null)
                {
                    MessageBox.Show(alunoAchado.NomeCompleto);
                    TextBoxNome.Text = alunoAchado.PrimeiroNome;
                    TextBoxIdAlundo.Text = alunoAchado.IdAluno.ToString();
                }
                else
                {
                    MessageBox.Show("Esse id de aluno não existe");
                  

                }
            }
            else
            {
                var alunoAchado = ProcurarAluno(TextBoxProcurar.Text);
                if (alunoAchado != null)
                {
                    MessageBox.Show("Este aluno Existe");
                    TextBoxNome.Text = alunoAchado.PrimeiroNome;
                    TextBoxIdAlundo.Text = alunoAchado.IdAluno.ToString();
                }
                else
                {
                    MessageBox.Show("Esse aluno não existe");

                }
            }
        }

        private Aluno ProcurarAluno(string nome)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if (nome == aluno.PrimeiroNome)
                {
                    return aluno;
                }
            }
            return null;
        }

        private Aluno ProcurarAluno(int id)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if(id == aluno.IdAluno)
                {
                    return aluno;
                }
            }
            return null;
        }

        private void buttonEditar_Click(object sender, EventArgs e)
        {
           
            TextBoxPrimeiroNome.Enabled = true;
            TextBoxApelidoAluno.Enabled = true;
            TextBoxIdAlundo.Enabled = false;
            ButtonGravar.Enabled = true;
            ButtonCancelar.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = false;
            TextBoxApelidoAluno.Enabled = false;
            TextBoxIdAlundo.Enabled = true;
            ButtonGravar.Enabled = false;
            ButtonCancelar.Enabled = false;

        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(TextBoxIdAlundo.Text))
            {
                MessageBox.Show("Insira id de Aluno");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Insira o primeiro Nome");
                return;
            }
            if (string.IsNullOrEmpty(TextBoxApelidoAluno.Text))
            {
                MessageBox.Show("Insira o Aplido do Aluno");
                return;
            }

            var alunoAchado = ProcurarAluno(Convert.ToInt32(TextBoxIdAlundo.Text));

            if(alunoAchado != null)
            {
                alunoAchado.PrimeiroNome = TextBoxPrimeiroNome.Text;
                alunoAchado.Apelido = TextBoxApelidoAluno.Text;
                MessageBox.Show("Aluno Alterado Com sucesso");

                actualizaCombos();

                TextBoxPrimeiroNome.Text = String.Empty;
                TextBoxPrimeiroNome.Enabled = false;

                TextBoxApelidoAluno.Text = String.Empty;
                TextBoxApelidoAluno.Enabled = false;



            }

           
        }

        private void actualizaCombos()
        {
            ComboBoxListaAlunos.DataSource = null;
            ComboNomes.Items.Clear();

            foreach (var aluno in ListaDeAlunos)
            {
                if(aluno.IdAluno.ToString() == TextBoxIdAlundo.Text)
                {
                    aluno.PrimeiroNome = TextBoxPrimeiroNome.Text;
                    aluno.Apelido = TextBoxApelidoAluno.Text;
                }
                ComboNomes.Items.Add(aluno.PrimeiroNome);
            }
            ComboBoxListaAlunos.DataSource = ListaDeAlunos;




        }



        private bool  GravarAlunos()
        {

            string ficheiro = @"alunos.txt";
           
            try
            {

                StreamWriter sw = new StreamWriter(ficheiro, false);
                if (!File.Exists(ficheiro))
                {
                    sw = File.CreateText(ficheiro);
                }

                foreach (var aluno in ListaDeAlunos)
                {
                    string linha = String.Format("{0};{1};{2};", aluno.IdAluno, aluno.PrimeiroNome, aluno.Apelido);
                    sw.WriteLine(linha);
                }
                sw.Close();

            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            
            
            return true;
        }


        private bool CarregarAlunos()
        {
            string ficheiro = @"alunos.txt";
           

            try
            {
               
                StreamReader sr = new StreamReader(ficheiro, true);
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);
                    string linha = String.Empty;
                    while((linha = sr.ReadLine()) != null)
                    {
                        String[] Campos = new String[3];
                        Campos = linha.Split(';');
                        var aluno = new Aluno
                        {
                            IdAluno = Convert.ToInt32(Campos[0]),
                            PrimeiroNome = Campos[1],
                            Apelido = Campos[2]
                        };

                        ListaDeAlunos.Add(aluno);
                    }
                    sr.Close();
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return true; 
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(GravarAlunos())
            {
                MessageBox.Show("Alunos gravados com sucesso");
            }
            
        }

        private void novoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            furmularioNovoAluno = new FormNovoAluno(ListaDeAlunos);
            furmularioNovoAluno.Show();

        }

        private void atualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizaCombos();
        }

        private void apagarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TextBoxIdAlundo.Text)){
                     MessageBox.Show("Insira id a procurar");
                return;
            }

            var AlunoApagado = ProcurarAluno(Convert.ToInt32(TextBoxIdAlundo.Text));
            formularioApagaraluno = new FormApagarAluno(ListaDeAlunos, AlunoApagado);
            formularioApagaraluno.Show();
        }
    }
}
