﻿namespace CRUDListaObj
{
    partial class FormApagarAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewAlunoApagar = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BotaoApagar = new System.Windows.Forms.Button();
            this.BotaoCancelar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAlunoApagar)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewAlunoApagar
            // 
            this.dataGridViewAlunoApagar.AllowUserToAddRows = false;
            this.dataGridViewAlunoApagar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewAlunoApagar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAlunoApagar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dataGridViewAlunoApagar.Location = new System.Drawing.Point(38, 27);
            this.dataGridViewAlunoApagar.Name = "dataGridViewAlunoApagar";
            this.dataGridViewAlunoApagar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridViewAlunoApagar.RowHeadersVisible = false;
            this.dataGridViewAlunoApagar.Size = new System.Drawing.Size(575, 150);
            this.dataGridViewAlunoApagar.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.HeaderText = "id Aluno";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Primeiro Nome";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Apelido";
            this.Column3.Name = "Column3";
            // 
            // BotaoApagar
            // 
            this.BotaoApagar.Image = global::CRUDListaObj.Properties.Resources.ic_ok_png;
            this.BotaoApagar.Location = new System.Drawing.Point(387, 201);
            this.BotaoApagar.Name = "BotaoApagar";
            this.BotaoApagar.Size = new System.Drawing.Size(110, 48);
            this.BotaoApagar.TabIndex = 1;
            this.BotaoApagar.UseVisualStyleBackColor = true;
            this.BotaoApagar.Click += new System.EventHandler(this.BotaoApagar_Click);
            // 
            // BotaoCancelar
            // 
            this.BotaoCancelar.Image = global::CRUDListaObj.Properties.Resources.ic_cancel;
            this.BotaoCancelar.Location = new System.Drawing.Point(503, 201);
            this.BotaoCancelar.Name = "BotaoCancelar";
            this.BotaoCancelar.Size = new System.Drawing.Size(110, 48);
            this.BotaoCancelar.TabIndex = 2;
            this.BotaoCancelar.UseVisualStyleBackColor = true;
            this.BotaoCancelar.Click += new System.EventHandler(this.BotaoCancelar_Click);
            // 
            // FormApagarAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 261);
            this.Controls.Add(this.BotaoCancelar);
            this.Controls.Add(this.BotaoApagar);
            this.Controls.Add(this.dataGridViewAlunoApagar);
            this.Name = "FormApagarAluno";
            this.Text = "FormApagarAluno";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAlunoApagar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewAlunoApagar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button BotaoApagar;
        private System.Windows.Forms.Button BotaoCancelar;
    }
}