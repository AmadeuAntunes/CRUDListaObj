﻿using CRUDListaObj.Modelos;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CRUDListaObj
{
    public partial class FormNovoAluno : Form
    {
        private List<Aluno> listaDeAlunos = new List<Aluno>();
        public FormNovoAluno(List<Aluno>listaDeAlunos)
        {
            InitializeComponent();
            this.listaDeAlunos = listaDeAlunos;
        }

        private void buttonnovoAluno_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Tem que inserir o primeiro nome do aluno");
                return;
            }
            if (string.IsNullOrEmpty(textBoxApelido.Text))
            {
                MessageBox.Show("Tem que inserir o apelido nome do aluno");
                return;
            }
            var aluno = new Aluno
            {
                IdAluno = GerarIdAluno(),
                PrimeiroNome = textBoxPrimeiroNome.Text,
                Apelido = textBoxApelido.Text
            };

            listaDeAlunos.Add(aluno);
            MessageBox.Show("Novo Aluno Criado Com sucesso!");
            Close();
        }

        private int GerarIdAluno()
        {
            return listaDeAlunos[listaDeAlunos.Count - 1].IdAluno + 10;
        }
    
    }
}
