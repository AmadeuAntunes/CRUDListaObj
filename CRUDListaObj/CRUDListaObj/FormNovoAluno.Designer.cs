﻿namespace CRUDListaObj
{
    partial class FormNovoAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.textBoxIDAluno = new System.Windows.Forms.TextBox();
            this.textBoxApelido = new System.Windows.Forms.TextBox();
            this.buttonnovoAluno = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Aluno: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro Nome: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apelido:";
            // 
            // textBoxPrimeiroNome
            // 
            this.textBoxPrimeiroNome.Location = new System.Drawing.Point(138, 103);
            this.textBoxPrimeiroNome.Name = "textBoxPrimeiroNome";
            this.textBoxPrimeiroNome.Size = new System.Drawing.Size(207, 20);
            this.textBoxPrimeiroNome.TabIndex = 3;
            // 
            // textBoxIDAluno
            // 
            this.textBoxIDAluno.Location = new System.Drawing.Point(138, 31);
            this.textBoxIDAluno.Name = "textBoxIDAluno";
            this.textBoxIDAluno.ReadOnly = true;
            this.textBoxIDAluno.Size = new System.Drawing.Size(207, 20);
            this.textBoxIDAluno.TabIndex = 4;
            // 
            // textBoxApelido
            // 
            this.textBoxApelido.Location = new System.Drawing.Point(138, 164);
            this.textBoxApelido.Name = "textBoxApelido";
            this.textBoxApelido.Size = new System.Drawing.Size(207, 20);
            this.textBoxApelido.TabIndex = 5;
            // 
            // buttonnovoAluno
            // 
            this.buttonnovoAluno.Image = global::CRUDListaObj.Properties.Resources.ic_ok_png;
            this.buttonnovoAluno.Location = new System.Drawing.Point(303, 222);
            this.buttonnovoAluno.Name = "buttonnovoAluno";
            this.buttonnovoAluno.Size = new System.Drawing.Size(123, 52);
            this.buttonnovoAluno.TabIndex = 6;
            this.buttonnovoAluno.UseVisualStyleBackColor = true;
            this.buttonnovoAluno.Click += new System.EventHandler(this.buttonnovoAluno_Click);
            // 
            // FormNovoAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 286);
            this.Controls.Add(this.buttonnovoAluno);
            this.Controls.Add(this.textBoxApelido);
            this.Controls.Add(this.textBoxIDAluno);
            this.Controls.Add(this.textBoxPrimeiroNome);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormNovoAluno";
            this.Text = "Novo Aluno";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPrimeiroNome;
        private System.Windows.Forms.TextBox textBoxIDAluno;
        private System.Windows.Forms.TextBox textBoxApelido;
        private System.Windows.Forms.Button buttonnovoAluno;
    }
}